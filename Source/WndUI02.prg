
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI02.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Maximizar
 *              - Minimizar
 *              - Titulo ventana
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"


Static   oWinUI
Static   oMainBar
Static   nRefresh          := 0

Function Main()
Local lEsMdi
Local lEnMdi
   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )                // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )             // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)


   SetResDebug( .T. )

   lEsMdi   := .T.
   lEnMdi   := .T.

   // 1.- Definicion de Ventana
   //     La definicion de ventanas MDI est� ajust�ndose
   //                        (lMax , lMdi, nColor, nStyl, oWnd, bWnd )
   oWinUI := TWindowsUI():New( .T., .T., lEsMdi,       ,      ,     ,      )

   // 2.- Definicion de Icono de Barra de Titulo y acciones de ventanas

   oWinUI:lDemoUI       := .F.
   oWinUI:lBttIconUI    := .T.
   oWinUI:lBttExitUI    := .T.
   oWinUI:lBttMaxiUI    := .T.   // Si la ventana es lMax, lo desactiva
   oWinUI:lBttMiniUI    := .T.
   oWinUI:lBttNormUI    := .T.   // Si la ventana es lMax, lo desactiva

   oWinUI:aBttIconUI     := { ".\Res\IconUI22.bmp", ".\Res\IconUI221.bmp" }
   oWinUI:aFilMaxiUI     := { ".\Res\path70901n.bmp", ".\Res\path70902.bmp" }
   oWinUI:aFilMiniUI     := { ".\Res\path70911n.bmp", ".\Res\path70912.bmp" }
   oWinUI:aFilNormUI     := { ".\Res\g71112n.bmp", ".\Res\g71122.bmp" }
   oWinUI:aBttExitUI     := { ".\Res\g60645n.bmp", ".\Res\g60646.bmp", 48, 20}

   // Si el entorno es lMdi, aun no se ha creado la TMDICHILD
   // Hay que ponerlo en el oWinUI:bTitUI ( Ver WndUI04.Prg ) porque si el
   // titulo es de la ventana principal o de otro control, hay que indicarlo
   // en su par�metro correspondiente.
   //
   // Para ver todas las posibilidades cambiar las variables locales lEsMdi y
   // lEnMdi

   if lEsMdi
      if !lEnMdi
   oWinUI:SetTitleUI( " Fivewin  /  Harbour 3.2.0 Dev ", -1, ,0 ,;
                      oWinUI:oWndUI  , ;
                     { METRO_WINDOW, METRO_APPWORKSPACE},oWinUI:oFontApliUI  )
      else
   oWinUI:bTitUI         := { | oW | ;
          oWinUI:SetTitleUI( " Fivewin  /  Harbour 3.2.0 Dev ", ;
           -1, ,0 ,, { METRO_APPWORKSPACE, METRO_WINDOW},oWinUI:oFontApliUI  ) }
      endif
   else
   oWinUI:SetTitleUI( " Fivewin Vers.  /  Harbour 3.2.0 Dev ", -1, ,0 ,, ;
                     { METRO_WINDOW, METRO_APPWORKSPACE},oWinUI:oFontApliUI  )

   endif
   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
/*
Function MiTitulo()
Local  cTit     := "Fivewin / Harbour"
Local  oSay
Local  nRow     := -1  //7
Local  nCol     := oWinUI:oWndUI:nWidth - 864

@ nRow, nCol SAY oSay PROMPT cTit OF oWinUI:oWndUI ; //:oWndClient ;
        FONT oWinUI:oFontApliUI ;
        PIXEL COLOR METRO_WINDOW, METRO_APPWORKSPACE ;
        SIZE GetTextWidth(0, cTit, oWinUI:oFontApliUI:hFont )+20, ;
             ( -( oWinUI:oFontApliUI:nInpHeight ) + 4 ) //;

Return oSay
*/
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
