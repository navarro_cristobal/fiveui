
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI14.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Entorno UI
 *              - Paneles laterales
 *              - Imagenes de en cuadro izquierdo
 *              - Panel Principal UI
 *              - Simulacion MDI
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "tselex.ch"

#include "colores.ch"

Static   oWinUI
Static   nRefresh          := 0

Function Main()
Local lMax    := .T.
Local lEsMdi  := .F.
Local nColor  := METRO_AZUL3
Local cTit    := "Inicio"
Local cUser   := WNetGetUser()
//Local aBtt    := { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }
Local aBtt    := { { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }, ;
                   { ".\Res\OnOff32.bmp", ".\Res\OnOff32.bmp" }, ;
                   { ".\Res\Busca32.bmp", ".\Res\Busca32.bmp" } }

Local oDlgs
Local oDlgR
Local oDlgL
Local oDlgT
Local oDlgB

Local bActions
Local aBmps1      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"      ;
                     }
Local aBmps2      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ;
                       }
Local aBmps3      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                     }

Local aBmps4      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp"  ;
                     }

Local aBmps5      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp" ;
                       }
Local aBmps6      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"  ;
                     }
Local aBmps7      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"  ;
                     }

Local aBmps8      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                     }

Local aBmps9      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                       }

Local aItems      := { ;// Deberia ser: {"Texto",cFnt,nHFnt,lBold,lItal,lLHT,lHHB,lLVL,lLVR}
                       {Upper("Double Click"),"Opcion 12","Opcion 13","Opcion 14",,},;
                       {"Opcion 21","Opcion 22","Opcion 23"},;
                       {},;
                       {"WNDUI 14"  ,"Opcion 32","Opcion 33"},;
                       {"Opcion 41","Opcion 42",,},;
                       {},;
                       {"Opcion 51","Opcion 52","Opcion 53","Opcion 54"},;
                       {"Opcion 61","Opcion 62","Opcion 63"},;
                       {},;
                       {"Opcion 71","Opcion 72","Opcion 73"},;
                       {"Opcion 81","Opcion 82","Opcion 83","Opcion 84"},;
                       {"Opcion 91","Opcion 92","Opcion 93"},;
                       {},;
                       {,,,};
                       }
                    /*

                       {},;
                       {,,,},;
                       {,,,},;
                       {,,,};
                      }
                    */

Local aBmps     := { aBmps1, aBmps2, , aBmps3, aBmps4, , ;
                       aBmps5, aBmps6, , aBmps7, aBmps8 , aBmps9 }

Local aTitGrps  := { "Grupo I","Grupo II","Grupo III","Grupo IV" }



   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )              // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )           // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   //---------------------------     -----------------------------------------//
   // Modelo 002:  Ventana: Panel principal
   //                       En ventana principal: NO MDI (No es necesario MDI)
   //              Definicion de cuadros de di�logos laterales
   //---------------------------     -----------------------------------------//

   oWinUI := TWindowsUI():PnelPPal( .T., lMax, lEsMdi, nColor, cTit, cUser, aBtt)

   // Activa la visualizacion de ventanas en panel izquierdo
   //
   oWinUI:lSaveBmp    := .T.

   oWinUI:lDlgRight   := .T.
   oWinUI:SRightUpUI( , , , , )

   oWinUI:lDlgLeft    := .T.
   oWinUI:SLeftUpUI( , , , , )

   oWinUI:lDlgBottom  := .F.
   oWinUI:lDlgTop     := .F.

   bActions := { | x, y, nF, oCol | XBrwMnu( oCol:oBrw:nRowSel, ;
                                             oCol:oBrw:nColSel, oCol )}
   // Simulacion de Menu "Azulejos"
   oWinUI:UIXPnel( oWinUI, aBmps, aTitGrps, 118, aItems, bActions )

   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

Function XBrwMnu( nRow, nCol, oCol )
Local nVentanas  :=  Len( oWinUI:GetListWnds() )
Local oWnd

Do Case
   Case nCol = 1
        Do Case
           Case nRow = 1
               //? nVentanas
               //cTitle, cMsg, oWnd, nColor, oFont1, lFlat, nWd, nHg
               TDialogUI():DlgMsgUI( "N� de Ventanas", Str( nVentanas ), , , , , ,)
           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 2
        Do Case
           Case nRow = 1

           Case nRow = 2
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 4
        Do Case
           Case nRow = 1
                oWnd   := Metro03()
                oWnd:ActivaUI()
           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 5
        Do Case
           Case nRow = 1

           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase


   Case nCol = 7
        Do Case
           Case nRow = 1
//                Editor()
                //? Len( oWinUI:HazListWnds() )

           Case nRow = 2
//                TextoVertical()
           Case nRow = 3
//                TextoHorizontal()
           Case nRow = 4
//                ObjectCells()
       EndCase

   Case nCol = 8
        Do Case
           Case nRow = 1
//                MetroUI02()
           Case nRow = 2
//                MultiLineCells()
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()
           Case nRow = 3
//                UsarAcces()
           Case nRow = 4
        EndCase

   Case nCol = 9
        Do Case
           Case nRow = 1
//                MetroUI02()
           Case nRow = 2
//                MultiLineCells()
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()
           Case nRow = 3
//                UsarAcces()
           Case nRow = 4
        EndCase

   Otherwise
        if Empty( nCol ) .or. nCol > Len( oCol:oBrw:aCols )
           ? "Columna no definida"
        endif
EndCase

Return nil


//----------------------------------------------------------------------------//

Function Metro03( )

   local oWnd
   local oDlg
   local oFont
   local oBrw
   local oFontS
   local x
   local hDC


   DEFINE FONT oFont NAME "Segoe UI Light" SIZE 0, -42  //-52

   //DEFINE FONT oFontS NAME "Segoe UI Symbol" SIZE 0, -32

   oWnd := TWindowsUI():New( .T., .T., .F., METRO_WINDOW,,,)

   WITH OBJECT oWnd

      :lSaveBmp        := .T.

      :lBttExitUI     := .T.
      :nRowBttUI      := 48
      :nColBttUI      := 48
      :aBttExitUI     := { ".\Res\back641.bmp", ".\Res\back642.bmp", 48, 48 }

      :lBttIconUI     := .T.
      :nRowBttIcoUI   := 78
      :nColBttIcoUI   := GetSysMetrics(0) - 90 //1200 // 290 //428
      :aBttIconUI    := { ".\Res\guest32.bmp", ".\Res\guest32.bmp", 32, 32 }
                                                        // { METRO_WINDOW, METRO_AZUL3 }
      :SetTitleUI( "Opcion 3.1" , 48, 330, 0, , { CLR_BLACK, METRO_WINDOW }, )
      :SetTUserUI( "Simulacion MDI 3.1", :nRowBttIcoUI-4, ;
                   GetSysMetrics(0) - 355, 0, , { CLR_BLACK, METRO_WINDOW }, ) //oFont )

      :oWndUI:bMMoved  := oWinUI:oWndUI:bMMoved
   END

   //oDlg  := HazDlg03( oWnd:oWndUI )
   oWnd:bControls := { | o | HazDlg03( o:oWndUI ) }
   //oWnd:oWndUI:bInit := { | o | HazDlg03( oWnd:oWndUI ) }

   oFont  := Nil
   oFontS := Nil

return oWnd


//----------------------------------------------------------------------------//

Function HazDlg03( oWnd )
Local oDlg
Local aOCtrl
        //TWindowsUI
oDlg := TDialogUI():NewDlg( .F. , .F., oWnd, 140, 330, 1360, 760,  )
oDlg:oWndUI:bInit := { | o | Controls03( oDlg ) }
oDlg:ActivaDlgUI(.F.)   //.F.

Return oDlg

//----------------------------------------------------------------------------//

Function Controls03( oDlg )
Local nFI       := 5 + 25
Local nFIG      := nFI + 20
Local nCI       := 185+100
Local nCIG      := nCI + 2
Local oFont
Local oFontX
Local nInc      := 40
Local nW        := 180
Local nH        := 24+2
Local oSel
Local nOpt1     := 1

Local oBttConex
Local oBttClose
Local oWnd
Local lSw        := .F.

   oWnd   := oDlg:oWndUI

   // Realizando c�lculos de ajuste de dialogos y ventanas
   // Solo son pruebas de momento
   nFI       := nFI  * GetDialogBaseUnits()[ 1 ]
   nFIG      := nFIG * GetDialogBaseUnits()[ 1 ]
   nCI       := (nCI - 100) * GetDialogBaseUnits()[ 2 ]
   nCIG      := (nCIG- 100) * GetDialogBaseUnits()[ 2 ]
   nInc      := nInc * GetDialogBaseUnits()[ 1 ]
   nW        := nW   * GetDialogBaseUnits()[ 2 ]
   nH        := nH   * GetDialogBaseUnits()[ 1 ]

//? GetDialogBaseUnits()[ 1 ], GetDialogBaseUnits()[ 2 ]

   DEFINE FONT oFont  NAME "Segoe UI Light" SIZE 0, -20  //-52
   DEFINE FONT oFontX NAME "Segoe UI SemiLight" SIZE 0, -14  //-52


   @ 10, 20 SELEX oSel VAR nOpt1 oF oWnd SIZE 60*2, 40*2 PIXEL ;
             ACTION ( MsgInfo("Si") ) ;
             ITEMS " ",  " " ;
             COLOR THUMB METRO_WINDOW ;
             GRADIENT INTRACK  {{ 1, METRO_GRIS1, METRO_GRIS1 },{ 0, METRO_GRIS1, METRO_GRIS1 }}  ;
             GRADIENT OUTTRACK {{ 1, METRO_WINDOW, METRO_WINDOW },{ 0, METRO_WINDOW, METRO_WINDOW }} ;
             LINECOLORS METRO_WINDOW, METRO_WINDOW ;
             ROUNDSIZE 0 ;
             THUMBSIZE 30, 30  //;




   @ nFI+nInc*6+10, 20 BUTTON oBttConex PROMPT " &ACEPTAR " SIZE 60*2, 20*2 PIXEL oF oWnd ;
      FONT oFontX ;
      ACTION ( MsgInfo("Aceptar" ) ) //CANCEL

   @ nFI+nInc*6+10, Int(20+109*1.5) BUTTON oBttClose PROMPT " &CANCELAR " SIZE 60*2, 20*2 PIXEL oF oWnd ;
      FONT oFontX ;
      ACTION ( MsgInfo("Cancelar" ) ) //CANCEL

Return  lSw    // Ha de devolver un array de controles ?

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
