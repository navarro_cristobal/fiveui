
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI17.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Maximizar
 *              - Minimizar
 *              - Titulo ventana
 *              - Paneles
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "tselex.ch"

#include "colores.ch"

#define CBS_NOINTEGRALHEIGHT  1024

Static   oWinUI
Static   nRefresh          := 0

Function Main()
Local lMax    := .T.
Local lEsMdi  := .F.
Local nColor  := METRO_AZUL3
Local cTit    := "Inicio"
Local cUser   := WNetGetUser()
//Local aBtt    := { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }
Local aBtt    := { { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }, ;
                   { ".\Res\OnOff32.bmp", ".\Res\OnOff32.bmp" }, ;
                   { ".\Res\Busca32.bmp", ".\Res\Busca32.bmp" } }

Local oDlgs
Local oDlgR
Local oDlgL
Local oDlgT
Local oDlgB

Local bActions
Local aBmps1      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"      ;
                     }
Local aBmps2      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ;
                       }
Local aBmps3      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                     }

Local aBmps4      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp"  ;
                     }

Local aBmps5      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp" ;
                       }
Local aBmps6      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"  ;
                     }
Local aBmps7      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"  ;
                     }

Local aBmps8      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                     }

Local aBmps9      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                       }

Local aItems      := { ;// Deberia ser: {"Texto",cFnt,nHFnt,lBold,lItal,lLHT,lHHB,lLVL,lLVR}
                       {"Opcion 11","Opcion 12","Opcion 13","Opcion 14",,},;
                       {"WndUI16"  ,"WndUI17"  ,"Opcion 23"},;
                       {},;
                       {"WndUI14"  ,"Opcion 32","Opcion 33"},;
                       {"WndUI15"  ,"Opcion 42",,},;
                       {},;
                       {"Opcion 51","Opcion 52","Opcion 53","Opcion 54"},;
                       {"Opcion 61","Opcion 62","Opcion 63"},;
                       {},;
                       {"Opcion 71","Opcion 72","Opcion 73"},;
                       {"Opcion 81","Opcion 82","Opcion 83","Opcion 84"},;
                       {"Opcion 91","Opcion 92","Opcion 93"},;
                       {},;
                       {,,,};
                       }
                    /*

                       {},;
                       {,,,},;
                       {,,,},;
                       {,,,};
                      }
                    */

Local aBmps     := { aBmps1, aBmps2, , aBmps3, aBmps4, , ;
                       aBmps5, aBmps6, , aBmps7, aBmps8 , aBmps9 }
Local aTitGrps  := { "Grupo I","Grupo II","Grupo III","Grupo IV" }



   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )              // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )           // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   //---------------------------     -----------------------------------------//
   // Modelo 002:  Ventana: Panel principal
   //                       En ventana principal: NO MDI (No es necesario MDI)
   //              Definicion de cuadros de di�logos laterales
   //---------------------------     -----------------------------------------//

   oWinUI := TWindowsUI():PnelPPal( .T., lMax, lEsMdi, nColor, cTit, cUser, aBtt)

   // Activa la visualizacion de ventanas en panel izquierdo
   //
   oWinUI:lSaveBmp    := .T.

   oWinUI:lDlgRight   := .T.
   oWinUI:SRightUpUI( , , , , )

   oWinUI:lDlgLeft    := .T.
   oWinUI:SLeftUpUI( , , , , )

   oWinUI:lDlgBottom  := .F.
   oWinUI:lDlgTop     := .F.

   bActions := { | x, y, nF, oCol | XBrwMnu( oCol:oBrw:nRowSel, ;
                                             oCol:oBrw:nColSel, oCol )}
   // Simulacion de Menu "Azulejos"
   oWinUI:UIXPnel( oWinUI, aBmps, aTitGrps, 118, aItems, bActions )

   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

Function CalculaRes( nTp )  // nTp -> 0 Ancho   nTp -> 1 Alto
local nAncho
local nAlto
local nPorcAnc
local nPorcAlt
DEFAULT nTp   := 0
      nAncho  := GetSysMetrics( 0 ) //ScreenWidth()  //GetSysmetrics( 4 )
      nAlto   := GetSysMetrics( 1 ) //ScreenHeight() //GetSysmetrics( 3 )
      if Empty( nTp )
         nPorcAnc := Round( ( nAncho/1366 ) , 4 )
      else
         nPorcAlt := Round( ( nAlto/768 ) , 4 )
      endif
     //    1920    1058      1920            1080               17                  17            22
     // ? nAncho, nAlto , GetSysmetrics(0), GetSysmetrics(1), GetSysmetrics(2),GetSysmetrics(3),GetSysmetrics(4)

Return IF( Empty( nTp ), nPorcAnc, nPorcAlt )

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
Function IsAppThemed() ; Return .F.
//----------------------------------------------------------------------------//
Function XBrwMnu( nRow, nCol, oCol )
Local nVentanas  :=  Len( oWinUI:GetListWnds() )
Local oWnd

Do Case
   Case nCol = 1
        Do Case
           Case nRow = 1
               //? nVentanas
               //cTitle, cMsg, oWnd, nColor, oFont1, lFlat, nWd, nHg
               TDialogUI():DlgMsgUI( "N� de Ventanas", Str( nVentanas ), , , , , ,)

           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 2
        Do Case
           Case nRow = 1
                oWnd  := Metro02( .F. )
                oWnd:ActivaUI()

           Case nRow = 2
                oWnd  := Metro02( .T. )
                oWnd:ActivaUI()

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 4
        Do Case
           Case nRow = 1
                oWnd   := Metro03( .T. )
                oWnd:ActivaUI()
           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 5
        Do Case
           Case nRow = 1
                oWnd   := Metro03( )
                oWnd:ActivaUI()

           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase


   Case nCol = 7
        Do Case
           Case nRow = 1
//                Editor()
                //? Len( oWinUI:HazListWnds() )

           Case nRow = 2
//                TextoVertical()
           Case nRow = 3
//                TextoHorizontal()
           Case nRow = 4
//                ObjectCells()
       EndCase

   Case nCol = 8
        Do Case
           Case nRow = 1
//                MetroUI02()
           Case nRow = 2
//                MultiLineCells()
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()
           Case nRow = 3
//                UsarAcces()
           Case nRow = 4
        EndCase

   Case nCol = 9
        Do Case
           Case nRow = 1
//                MetroUI02()
           Case nRow = 2
//                MultiLineCells()
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()
           Case nRow = 3
//                UsarAcces()
           Case nRow = 4
        EndCase

   Otherwise
        if Empty( nCol ) .or. nCol > Len( oCol:oBrw:aCols )
           ? "Columna no definida"
        endif
EndCase

Return nil


//----------------------------------------------------------------------------//

Function Metro03( lMnu )

   local oWnd
   local oDlg
   local oFont
   local oBrw
   local oFontS
   local x
   local hDC

   DEFAULT lMnu   := .F.


   DEFINE FONT oFont NAME "Segoe UI Light" SIZE 0, -42  //-52

   //DEFINE FONT oFontS NAME "Segoe UI Symbol" SIZE 0, -32

   oWnd := TWindowsUI():New( .T., .T., .F., METRO_WINDOW,,,)

   WITH OBJECT oWnd

      :lSaveBmp        := .T.

      :lBttExitUI     := .T.
      :nRowBttUI      := 48
      :nColBttUI      := 48
      :aBttExitUI     := { ".\Res\back641.bmp", ".\Res\back642.bmp", 48, 48 }

      :lBttIconUI     := .T.
      :nRowBttIcoUI   := 78
      :nColBttIcoUI   := GetSysMetrics(0) - 90 //1200 // 290 //428
      :aBttIconUI    := { ".\Res\guest32.bmp", ".\Res\guest32.bmp", 32, 32 }

      if lMnu
      :SetTitleUI( "Opcion 3.1" , 48, 330, 0, , { CLR_BLACK, METRO_WINDOW }, )
      :SetTUserUI( "Simulacion MDI 3.1", :nRowBttIcoUI-4, ;
                   GetSysMetrics(0) - 355, 0, , { CLR_BLACK, METRO_WINDOW }, ) //oFont )
      else
      :SetTitleUI( "Opcion 4.1" , 48, 330, 0, , { CLR_BLACK, METRO_WINDOW }, )
      :SetTUserUI( "Simulacion MDI 4.1", :nRowBttIcoUI-4, ;
                   GetSysMetrics(0) - 355, 0, , { CLR_BLACK, METRO_WINDOW }, ) //oFont )

      endif
      :oWndUI:bMMoved  := oWinUI:oWndUI:bMMoved
   END

   if lMnu
      oBrw  := HazBrowse03( oWnd:oWndUI, oFont )
   endif

   //oDlg  := HazDlg03( oWnd:oWndUI )
   oWnd:bControls := { | o | HazDlg03( o:oWndUI ) }
   //oWnd:oWndUI:bInit := { | o | HazDlg03( oWnd:oWndUI ) }

   oFont  := Nil
   oFontS := Nil

return oWnd


//----------------------------------------------------------------------------//

Function HazDlg03( oWnd )
Local oDlg
Local aOCtrl
        //TWindowsUI
oDlg := TDialogUI():NewDlg( .F. , .F., oWnd, 140, 330, 1360, 760,  )
oDlg:oWndUI:bInit := { | o | Controls03( oDlg ) }
oDlg:ActivaDlgUI(.F.)   //.F.

Return oDlg

//----------------------------------------------------------------------------//

Function Controls03( oDlg )
Local nFI       := 5 + 25
Local nFIG      := nFI + 20
Local nCI       := 185+100
Local nCIG      := nCI + 2
Local oFont
Local oFontX
Local nInc      := 40
Local nW        := 180
Local nH        := 24+2
Local oSel
Local nOpt1     := 1

Local oBttConex
Local oBttClose
Local oWnd
Local lSw        := .F.

   oWnd   := oDlg:oWndUI

   // Realizando c�lculos de ajuste de dialogos y ventanas
   // Solo son pruebas de momento
   nFI       := nFI  * GetDialogBaseUnits()[ 1 ]
   nFIG      := nFIG * GetDialogBaseUnits()[ 1 ]
   nCI       := (nCI - 100) * GetDialogBaseUnits()[ 2 ]
   nCIG      := (nCIG- 100) * GetDialogBaseUnits()[ 2 ]
   nInc      := nInc * GetDialogBaseUnits()[ 1 ]
   nW        := nW   * GetDialogBaseUnits()[ 2 ]
   nH        := nH   * GetDialogBaseUnits()[ 1 ]

//? GetDialogBaseUnits()[ 1 ], GetDialogBaseUnits()[ 2 ]

   DEFINE FONT oFont  NAME "Segoe UI Light" SIZE 0, -20  //-52
   DEFINE FONT oFontX NAME "Segoe UI SemiLight" SIZE 0, -14  //-52


   @ 10, 20 SELEX oSel VAR nOpt1 oF oWnd SIZE 60*2, 40*2 PIXEL ;
             ACTION ( MsgInfo("Si") ) ;
             ITEMS " ",  " " ;
             COLOR THUMB METRO_WINDOW ;
             GRADIENT INTRACK  {{ 1, METRO_GRIS1, METRO_GRIS1 },{ 0, METRO_GRIS1, METRO_GRIS1 }}  ;
             GRADIENT OUTTRACK {{ 1, METRO_WINDOW, METRO_WINDOW },{ 0, METRO_WINDOW, METRO_WINDOW }} ;
             LINECOLORS METRO_WINDOW, METRO_WINDOW ;
             ROUNDSIZE 0 ;
             THUMBSIZE 30, 30  //;




   @ nFI+nInc*6+10, 20 BUTTON oBttConex PROMPT " &ACEPTAR " SIZE 60*2, 20*2 PIXEL oF oWnd ;
      FONT oFontX ;
      ACTION ( MsgInfo("Aceptar" ) ) //CANCEL

   @ nFI+nInc*6+10, Int(20+109*1.5) BUTTON oBttClose PROMPT " &CANCELAR " SIZE 60*2, 20*2 PIXEL oF oWnd ;
      FONT oFontX ;
      ACTION ( MsgInfo("Cancelar" ) ) //CANCEL

Return  lSw    // Ha de devolver un array de controles ?

//----------------------------------------------------------------------------//

Function HazBrowse03( oWnd, oFont )
Local oBrw
Local aItems := { "Opcion Primera", "Segunda Opcion", "Tercera Opcion", ;
                  "Cuarta Opcion", "Quinta Opcion", "Sexta Opcion", "Septima" }

Local oFont2
Local oFont3

   DEFINE FONT oFont2 NAME "Segoe UI Light" SIZE 0, -32
   DEFINE FONT oFont3 NAME "Segoe UI Light" SIZE 0, -16

   @ 140, 12 XBROWSE oBrw ARRAY aItems COLSIZES 310 CELL ;
      FONT oFont2 SIZE 315, 645 PIXEL NOBORDER OF oWnd

   WITH OBJECT oBrw

        :nDataLines = 2
        :nRowHeight = 90
        :lRecordSelector = .F.
        :lHeader   = .F.
        :lHScroll  = .F.
        :lVScroll  = .F.
        :nStretchCol = 1
        :bClrStd = { || { CLR_BLACK, CLR_WHITE } }
        :bClrSel = { || { CLR_WHITE, METRO_GRAYTEXT } }
        :bClrSelFocus = { || { CLR_WHITE, RGB( 34, 177, 76 ) } }

        //:SetColor( METRO_WINDOW, METRO_GRIS1 )   //"B/W*" )
        :SetArray( aItems )
        :aCols[1]:bLDClickData   := {|x,y,nF,oCol| OpcBrw03( oBrw:nArrayAt, ;
                                  oBrw, x, y, oBrw:nRowSel, oBrw:nColSel, nF) }
        :bKeyChar   := :aCols[1]:bLDClickData

        :CreateFromCode()
        :aCols[ 1 ]:bPaintText = { | oCol,hDC,cText,aCoors,aColors,lHighlight |;
                                DrawRowN( oCol, hDC, cText, aCoors , oFont3 )  }
   END

Return oBrw

//----------------------------------------------------------------------------//

Static function DrawRowN( oCol, hDC, cText, aCoors, oFont )

   local hOldFont
   local aItems := { "Esta es la Primera Opcion",;
                     "Pulsa aqui para la Segunda",;
                     "Tercer comentario",;
                     "Opcion Cuarta del Menu",;
                     "Pulsa Utilidades y Varios",;
                     "Otras opciones",;
                     "Salir del Menu" }

   DrawText( hDC, cText, aCoors )
   aCoors[ 1 ] += 45
   //aCoors[ 2 ] += 12
   hOldFont = SelectObject( hDC, oFont:hFont )
   DrawText( hDC, aItems[ oCol:oBrw:KeyNo ], aCoors )
   SelectObject( hDC, hOldFont )

return nil

//----------------------------------------------------------------------------//

Function OpcBrw03( nPos, oBrw, x, y, nRow, nCol, nF )
Local oDlg
Local oWin
Local cFile     := ""
Local  aCampos  := {}
Do Case
   Case nCol = 1
        Do Case
           Case nRow = 1
                //

           Case nRow = 2
                //
                oWin := Metro03()
                oWin:ActivaUI()
           Case nRow = 3
                //

           Case nRow = 4
                //
           Case nRow = 5
                //
                oWin := Metro03()
                oWin:ActivaUI()

           Case nRow = 6

           Case nRow = 7

         EndCase
   Otherwise

EndCase

Return nil

//----------------------------------------------------------------------------//

Function Metro02( lMnu )

   local oWnd
   local oDlg
   local oFont
   local oBrw
   local oFontS
   local x
   local hDC

   DEFAULT lMnu   := .F.


   DEFINE FONT oFont NAME "Segoe UI Light" SIZE 0, -42  //-52

   //DEFINE FONT oFontS NAME "Segoe UI Symbol" SIZE 0, -32

   oWnd := TWindowsUI():New( .T., .T., .F., METRO_WINDOW,,,)

   WITH OBJECT oWnd

      :lSaveBmp        := .T.

      :lBttExitUI     := .T.
      :nRowBttUI      := 48
      :nColBttUI      := 48
      :aBttExitUI     := { ".\Res\back641.bmp", ".\Res\back642.bmp", 48, 48 }

      :lBttIconUI     := .T.
      :nRowBttIcoUI   := 78
      :nColBttIcoUI   := GetSysMetrics(0) - 90 //1200 // 290 //428
      :aBttIconUI    := { ".\Res\guest32.bmp", ".\Res\guest32.bmp", 32, 32 }

      if lMnu
      :SetTitleUI( "Opcion 3.1" , 48, 330, 0, , { CLR_BLACK, METRO_WINDOW }, )
      :SetTUserUI( "Simulacion MDI 3.1", :nRowBttIcoUI-4, ;
                   GetSysMetrics(0) - 355, 0, , { CLR_BLACK, METRO_WINDOW }, ) //oFont )
      else
      :SetTitleUI( "Opcion 2.1" , 48, 330, 0, , { CLR_BLACK, METRO_WINDOW }, )
      :SetTUserUI( "Simulacion MDI 2.1", :nRowBttIcoUI-4, ;
                   GetSysMetrics(0) - 355, 0, , { CLR_BLACK, METRO_WINDOW }, ) //oFont )

      endif
      :oWndUI:bMMoved  := oWinUI:oWndUI:bMMoved
   END

   if lMnu
      oBrw  := HazBrowse03( oWnd:oWndUI, oFont )
   endif

   //oDlg  := HazDlg02( oWnd:oWndUI )
   oWnd:bControls := { | o | HazDlg02( o:oWndUI ) }
   //oWnd:oWndUI:bInit := { | o | HazDlg02( oWnd:oWndUI ) }

   oFont  := Nil
   oFontS := Nil

return oWnd

//----------------------------------------------------------------------------//

Function HazDlg02( oWnd )
Local oDlg
Local aOCtrl

oDlg := TDialogUI():NewDlg( .F. , .F., oWnd, 140, 330, 1360, 760,  )

oDlg:oWndUI:bInit := { | o | aOCtrl := Controls02( oDlg ), ;
                             CtrlPnel1( aOCtrl[1] ), CtrlPnel2( aOCtrl[2] ) }
//aOCtrl   := Controls02( oDlg )
//oDlg:oWndUI:bInit := { | o | CtrlPnel2( aOCtrl[2] ), CtrlPnel1( aOCtrl[1] ) }
oDlg:ActivaDlgUI(.F.)

Return oDlg

//----------------------------------------------------------------------------//

Function Controls02( oDlg )  //oPnel1, oPnel2 )
Local nFI       := 5 + 25
Local nFIG      := nFI + 20
Local nCI       := 185+100
Local nCIG      := nCI + 2
Local oFont
Local oFontX
Local nInc      := 40
Local nW        := 120 * 1.60
Local nH        := 40 * 1.60
Local oSelJet
Local cSelJet   := "Cadena de Mensaje                   "

Local oSay0
Local oSay1
Local oSay2
Local oSay3
Local oSel1
Local nOpt1     := 1
Local oPanel1
Local oPanel2
Local nAltoSpl1  := 329
Local nAnchoSpl1 := 140  //180
Local oBttConex
Local oBttClose
Local oWnd
Local lSw        := .F.

   oWnd        := oDlg:oWndUI
   nAltoSpl1   := oWnd:nHeight
   nAnchoSpl1  := oWnd:nWidth / 3
   nCI         := 695
   nCIG        := nCI + 2
   DEFINE FONT oFont  NAME "Segoe UI Light" SIZE 0, -20  //-52
   DEFINE FONT oFontX NAME "Segoe UI SemiLight" SIZE 0, -14  //-52

   oPanel1 := TPanel():New( 0, 0, nAltoSpl1, nAnchoSpl1, oWnd )
   oPanel1:SetColor( METRO_GRIS2, METRO_GRIS1 )

   oPanel2 := TPanel():New( 0, nAnchoSpl1, nAltoSpl1, nAnchoSpl1*2, oWnd )
   oPanel2:SetColor( METRO_GRIS1, METRO_GRIS4 )

   @ nFI-25 , nCI SAY oSay0 ;
     PROMPT " Cuadro de Dialogo: "  ;
     SIZE nW, nH PIXEL oF oWnd ;
     COLOR METRO_WINDOW, METRO_GRIS2 FONT oFont TRANSPARENT

   @ nFI+nInc*1, nCI SAY " Mensaje: " ;
     SIZE nW, nH PIXEL oF oWnd ;
     COLOR METRO_WINDOW, METRO_GRIS2 FONT oFont TRANSPARENT

   @ nFI+nInc*2, nCI GET oSelJet VAR cSelJet SIZE nW*1.45, nH/2 PIXEL oF oWnd ;
      FONT oFontX ;
      COLOR METRO_GRIS2, METRO_WINDOW ;   //METRO_APPWORKSPACE, METRO_WINDOW
      PICTURE "@!"  ;
      VALID ( .T. )

   @ nFI+nInc*13, nCIG BUTTON oBttConex PROMPT " &Aceptar " ;
                  SIZE 160, 40 PIXEL oF oWnd FONT oFontX

   @ nFI+nInc*13, Int(nCIG+109*1.5) BUTTON oBttClose PROMPT " &Cancelar " ;
                  SIZE 160, 40 PIXEL oF oWnd FONT oFontX


   //oFont:End()
   //oFont    := Nil

Return { oPanel1, oPanel2 }

//----------------------------------------------------------------------------//

Function CtrlPnel1( oPanel1 )
Local oFontX
Local nW        := 160 * 1.60
Local oTipos
Local cTipoEx   := "Mover al Principio"
Local oTipos1
Local cTipoEx1  := "Mover al Final"
Local aTipo     := {"Eliminar", "Insertar Antes", ;
                    "Insertar Despu�s", "Mover Abajo", ;
                    "Mover Arriba","Mover al Principio", ;
                    "Mover al Final" }

   DEFINE FONT oFontX NAME "Segoe UI SemiLight" SIZE 0, -14  //-52

   @ 4, 4 COMBOBOX oTipos VAR cTipoEx ;
     ITEMS aTipo ;
     OF oPanel1 ;
     SIZE Int(nW*0.68), 190 ;   //275
     FONT oFontX ;
     STYLE CBS_DROPDOWNLIST ;
     PIXEL COLOR METRO_GRIS1, METRO_WINDOW

     oTipos:nStyle := nOr( oTipos:nStyle, CBS_NOINTEGRALHEIGHT )

   @ 590, 162 COMBOBOX oTipos1 VAR cTipoEx1 ;
     ITEMS aTipo ;
     OF oPanel1 ;
     SIZE Int(nW*0.68), 190 ;   //275
     FONT oFontX ;
     STYLE CBS_DROPDOWNLIST ;
     PIXEL COLOR METRO_GRIS1, METRO_WINDOW

     oTipos1:nStyle := nOr( oTipos1:nStyle, CBS_NOINTEGRALHEIGHT )

Return nil

//----------------------------------------------------------------------------//

Function CtrlPnel2( oPanel2 )
Local oFontX
Local oFont
Local nW        := 160 * 1.60
Local nH        := 19 * 1.60
Local oTipos
Local aTipo     := {"Eliminar", "Insertar Antes", ;
                    "Insertar Despu�s", "Mover Abajo", ;
                    "Mover Arriba","Mover al Principio", ;
                    "Mover al Final" }

   DEFINE FONT oFont  NAME "Segoe UI Light" SIZE 0, -20  //-52
   DEFINE FONT oFontX NAME "Segoe UI SemiLight" SIZE 0, -14  //-52

   @ 4, 4 XBROWSE oTipos ;
     ARRAY aTipo ; // COLSIZES (nW-6) ;      CELL ;
     FONT oFontX ;
     SIZE nW-4, Int(nH)*11 - 20 PIXEL oF oPanel2 //NOBORDER

   WITH OBJECT oTipos
        :lHScroll        := .F.
        :lVScroll        := .F.
        :lHeader         := .T.
        :lRecordSelector := .F.
        :l2007           := .F.

        :bClrSel         := { || { METRO_WINDOWTEXT, METRO_WINDOW } }
        :bClrStd         := { || { METRO_WINDOWTEXT, METRO_WINDOW } }
        :bClrSelFocus    := { || { METRO_WINDOW, METRO_APPWORKSPACE } }

        :bClrHeader      := { || { METRO_WINDOW, METRO_GRIS4 } }
        :SetColor( METRO_WINDOWTEXT, METRO_WINDOW )

        :nRowDividerStyle  := LINESTYLE_NOLINES        //4
        :nColDividerStyle  := LINESTYLE_NOLINES        //4
        :nMarqueeStyle     := 5                        //3
        :nRowHeight        := 38
        :lAutoSort         := .F.

        :SetArray( aTipo )

   END

   WITH OBJECT oTipos:aCols[ 1 ]

      :nWidth         := nW-4
      :cHeader        := " Opciones "
      :nDataStrAlign  := 0
      :lAllowSizing   := .F.
      :oHeaderFont    := oFont

   END

   oTipos:CreateFromCode()

Return nil

//----------------------------------------------------------------------------//
